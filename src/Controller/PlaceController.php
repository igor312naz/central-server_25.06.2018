<?php

namespace App\Controller;


use App\Model\Place\PlaceHandler;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlaceController extends Controller
{
    /**
     * @Route("/place", name="place")
     * @param PlaceHandler $placeHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function placeAction(
        PlaceHandler $placeHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['type'] = $request->request->get('type');
        $data['landlord'] = $request->request->get('landlord');
        $data['placeName'] = $request->request->get('placeName');
        $data['address'] = $request->request->get('address');
        $data['price'] = $request->request->get('price');

        if(empty($data['name']) || empty($data['landlord']) || empty($data['price'])) {
            return new JsonResponse(['error' => 'error'.var_export($data,1)],406);
        }

        $place = $placeHandler->createNewRestPlace($data);

        $manager->persist($place);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }


}

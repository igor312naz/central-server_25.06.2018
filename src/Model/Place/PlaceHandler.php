<?php


namespace App\Model\Place;

use App\Entity\Hotel;
use App\Entity\Place;
use App\Entity\Sanatorium;

class PlaceHandler
{


    /**
     * @param array $data
     * @return Place
     */
    public function createNewRestPlace(array $data) {
        $restPlace = new Place();
        $this->createNewAbstractRestPlace($restPlace, $data);

        return $restPlace;
    }

    /**
     * @param Place $place
     * @param array $data
     * @return Place
     */
    public function createNewAbstractRestPlace(Place $place, array $data) {
        $place->setLandlord($data['landlord']);
        $place->setPlaceName($data['placeName']);
        $place->setAddress($data['address']);
        $place->setPrice($data['price']);

        return $place;
    }

    /**
     * @param array $data
     * @return Sanatorium
     */
    public function createNewSanatorium(array $data) {
        /** @var Sanatorium $place */
        $place = $this->createNewAbstractRestPlace(new Sanatorium(), $data);
        $place->setHealingProcedures($data['healing_procedures'] ?? 0);
        $place->setMassage($data['massage'] ?? 0);

        return $place;
    }

    /**
     * @param array $data
     * @return Hotel
     */
    public function createNewHotel(array $data) {
        /** @var Hotel $place */
        $place = $this->createNewAbstractRestPlace(new Hotel(), $data);
        $place->setBathroom($data['bathroom'] ?? 0);
        $place->setKitchen($data['kitchen'] ?? 0);

        return $place;
    }
}

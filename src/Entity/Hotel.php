<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel extends Place
{
    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $kitchen;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $bathroom;

    /**
     * @param int $kitchen
     * @return Hotel
     */
    public function setKitchen(int $kitchen): Hotel
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return int
     */
    public function getKitchen(): int
    {
        return $this->kitchen;
    }

    /**
     * @param int $bathroom
     * @return Hotel
     */
    public function setBathroom(int $bathroom): Hotel
    {
        $this->bathroom = $bathroom;
        return $this;
    }

    /**
     * @return int
     */
    public function getBathroom(): int
    {
        return $this->bathroom;
    }
}
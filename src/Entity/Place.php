<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"place" = "Place", "sanatorium" = "Sanatorium", "hotel" = "Hotel"})
 */
class Place
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Landlord", inversedBy="places")
     */
    private $landlord;

    /**
     * @var string
     * @ORM\Column(type="string", length=254)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $placeName;


    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $address;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $price;

    /**
     * @param int $id
     * @return Place
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $landlord
     * @return Place
     */
    public function setLandlord($landlord)
    {
        $this->landlord = $landlord;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLandlord()
    {
        return $this->landlord;
    }

    /**
     * @param string $type
     * @return Place
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $placeName
     * @return Place
     */
    public function setPlaceName(string $placeName)
    {
        $this->placeName = $placeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceName()
    {
        return $this->placeName;
    }

    /**
     * @param string $address
     * @return Place
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param int $price
     * @return Place
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }


}
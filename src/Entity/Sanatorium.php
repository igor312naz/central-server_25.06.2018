<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SanatoriumRepository")
 */
class Sanatorium extends Place
{
    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $massage;

    /**
     * @var int
     * @ORM\Column(type="integer", length=11)
     */
    private $healingProcedures;




    /**
     * @param int $massage
     * @return Sanatorium
     */public function setMassage(int $massage): Sanatorium
{
    $this->massage = $massage;
    return $this;
}

    /**
     * @return int
     */
    public function getMassage(): int
    {
        return $this->massage;
    }

    /**
     * @param int $healingProcedures
     * @return Sanatorium
     */
    public function setHealingProcedures(int $healingProcedures): Sanatorium
    {
        $this->healingProcedures = $healingProcedures;
        return $this;
    }

    /**
     * @return int
     */
    public function getHealingProcedures(): int
    {
        return $this->healingProcedures;
    }


}
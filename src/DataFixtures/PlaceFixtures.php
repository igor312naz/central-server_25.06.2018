<?php
namespace App\DataFixtures;

use App\Entity\Client;
use App\Model\Place\PlaceHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PlaceFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var PlaceHandler
     */
    private $placeHandler;

    public function __construct(PlaceHandler $placeHandler)
    {
        $this->placeHandler = $placeHandler;
    }

    public function load(ObjectManager $manager)
    {
        $landlord = $this->getReference('client1');

        $place = $this->placeHandler->createNewSanatorium([
            'landlord' => '1',
            'placeName' => 'qqqq',
            'address' => 'bbbb',
            'price' => '123',
        ]);

        $manager->persist($place);
        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            ClientFixtures::class,

        );

    }
}
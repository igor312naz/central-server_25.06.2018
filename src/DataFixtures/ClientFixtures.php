<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {

        $client1 = $this->clientHandler->createNewTenant([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
            'credit_card' => '123',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ]);

        $this->addReference('client1', $client1);
        $manager->persist($client1);

        $manager->flush();
    }

}
